package com.enterprise.adri.gescream;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class IceCreamActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ice_cream);

        // Inisialisasi

        ImageButton buttonDelivery = (ImageButton) findViewById(R.id.deliveryButton);
        ImageButton buttonDitempat = (ImageButton) findViewById(R.id.ditempatButton);

        buttonDitempat.setOnClickListener(new ButtonOrder());

    }

    private class ButtonOrder implements Button.OnClickListener{
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getBaseContext(),OrderActivity.class);
            startActivity(intent);
        }
    }
}
