package com.enterprise.adri.gescream;

/**
 * Copyright GesCream IT Departement
 */

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;



public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Inisialisasi
        ImageButton buttonPromosi = (ImageButton) findViewById(R.id.promosiButton);
        ImageButton buttonIceCream = (ImageButton) findViewById(R.id.iceCreamButton);

        // Set onClickListener
        buttonPromosi.setOnClickListener(new buttonChangeActivity());
        buttonIceCream.setOnClickListener(new buttonChangeActivity());





    }
    // On Click Listener untuk setiap button
    private class buttonChangeActivity implements Button.OnClickListener{
        @Override
        public void onClick(View v) {
            int id =  v.getId();
            switch (id){
                case R.id.iceCreamButton:
                    Intent intentIceCream = new Intent(getBaseContext(),IceCreamActivity.class);
                    startActivity(intentIceCream);
                    break;
                case R.id.promosiButton:
                    Intent intentPromosi = new Intent(getBaseContext(),PromosiActivity.class);
                    startActivity(intentPromosi);
                    break;
            }
        }
    }

}

