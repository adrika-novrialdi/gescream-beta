package com.enterprise.adri.gescream;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class OrderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        // Inisialisasi

        ImageButton king = (ImageButton) findViewById(R.id.king);
        ImageButton witch = (ImageButton) findViewById(R.id.witch);
        ImageButton princess = (ImageButton) findViewById(R.id.princess);
        ImageButton queen = (ImageButton) findViewById(R.id.queen);
        ImageButton fairy = (ImageButton) findViewById(R.id.fairy);
        ImageButton knight = (ImageButton) findViewById(R.id.knight);
        ImageButton prince = (ImageButton) findViewById(R.id.prince);
        ImageButton ichigo = (ImageButton) findViewById(R.id.ichigo);

        ImageButton[] imageButtons = {king,witch,princess,queen,fairy,knight,prince,ichigo};

        for (ImageButton imageButton : imageButtons){
            imageButton.setOnClickListener(new SelectIce());
        }






    }
    private class SelectIce implements Button.OnClickListener{
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getBaseContext(),ToppingActivity.class);
            String namaIce = "";
            int harga = 0;
            switch (v.getId()){
                case R.id.king:
                    namaIce = " King Banana Split";
                    harga = 15000;
                    break;
                case R.id.witch:
                    namaIce = "Witch Fruity Bizarre";
                    harga = 15000;
                    break;
                case R.id.princess:
                    namaIce = "Princess Cony Ice";
                    harga = 3000;
                    break;
                case R.id.queen:
                    namaIce = "Queen Flavour";
                    harga = 6000;
                    break;
                case R.id.fairy:
                    namaIce = "Fairy Castella";
                    harga = 8000;
                    break;
                case R.id.knight:
                    namaIce = "Knight Toffee";
                    harga = 10000;
                    break;
                case R.id.prince:
                    namaIce = "Prince Smores";
                    harga = 10000;
                    break;
                case R.id.ichigo:
                    namaIce ="Ichigo Daifuku";
                    harga = 15000;
                    break;

            }
            intent.putExtra("NamaEs",namaIce);
            intent.putExtra("HargaEs",String.valueOf(harga));
            startActivity(intent);
        }
    }
}
