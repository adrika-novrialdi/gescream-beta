package com.enterprise.adri.gescream;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;


import java.util.HashMap;

public class ToppingActivity extends AppCompatActivity {

HashMap<String,String> simpanData = new HashMap<>();
int hargaAsli = 0;
int hargaSetelahTopping = 0;
boolean isAdaToping = false;
boolean isExplored = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topping);

        // Dapatkan data Es krim yang dipesan
        Intent intent = getIntent();
        String content = intent.getExtras().getString("NamaEs");
        final String hargaEs = intent.getExtras().getString("HargaEs");
        hargaAsli = Integer.parseInt(hargaEs);

        // Masukkan Nama dan Harga ke List ( Indeks 0 dan 1)
        simpanData.put("Nama",content);
        simpanData.put("Harga",hargaEs);




        // Button dan text view konten atas
        Button increment = (Button) findViewById(R.id.buttonIncrement);
        Button decrement = (Button) findViewById(R.id.buttonDecrement);
        TextView jumlahPesanan = (TextView) findViewById(R.id.jumlahPesanan);




        increment.setOnClickListener(new ButtonToppingActivityHandler());
        decrement.setOnClickListener(new ButtonToppingActivityHandler());
        String testHarga = String.valueOf(hargaEs);







        //implementasi button toping (untuk memesan)
        Spinner spinnerTopping = (Spinner) findViewById(R.id.spinneruntukTopping) ;
        ArrayAdapter<CharSequence> adapterTopping = ArrayAdapter.createFromResource(this
                ,R.array.toppinglist,R.layout.spinner_item);
        adapterTopping.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTopping.setAdapter(adapterTopping);

        spinnerTopping.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int hargaSekarang = 0;
                String topping = (parent.getItemAtPosition(position).toString());
                simpanData.put("Topping",topping);
                if (!topping.equalsIgnoreCase("-")){
                    hargaSetelahTopping = Integer.parseInt(simpanData.get("HargaLast"));
                    simpanData.put("Harga",String.valueOf(hargaSetelahTopping+1000));
                    hargaSetelahTopping+=1000;
                    isAdaToping = true;
                    isExplored = true;

                }else {
                    simpanData.put("Topping","-");
                    String harganya = simpanData.get("HargaLast");
                    if (harganya!=null) hargaSetelahTopping = Integer.parseInt(harganya);
                    else hargaSetelahTopping = hargaAsli;
                    if (isExplored) simpanData.put("Harga",String.valueOf(hargaSetelahTopping));
                    else simpanData.put("Harga",String.valueOf(hargaSetelahTopping-1000));
                    isAdaToping = false;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Implementasi rasa
        Spinner spinnerRasa = (Spinner) findViewById(R.id.spinneruntukRasa) ;
        ArrayAdapter<CharSequence> adapterRasa = ArrayAdapter.createFromResource(this
                ,R.array.rasaList,R.layout.spinner_item);
        adapterRasa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerRasa.setAdapter(adapterRasa);

        spinnerRasa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int hargaSekarang = 0;
                int hargaAsliNow = 0 ;
                if (!isAdaToping){
                    hargaAsliNow = hargaAsli;
                }else hargaAsliNow = hargaSetelahTopping;


                if (position==3 || position==4 || position==10|| position==11){
                     hargaSekarang = hargaAsliNow;
                     hargaSekarang+=1000;
                     simpanData.put("Harga",String.valueOf(hargaSekarang));
                    simpanData.put("HargaLast",String.valueOf(hargaSekarang));

                     Log.d("Masuk mana","Kumpulan harga plus 1000");
                     Log.d("Harga",String.valueOf(hargaSekarang));

                } else if (position==6){
                    hargaSekarang = hargaAsliNow;
                    hargaSekarang+=2000;
                    simpanData.put("Harga",String.valueOf(hargaSekarang));
                    simpanData.put("HargaLast",String.valueOf(hargaSekarang));
                    Log.d("Masuk mana","Kumpulan harga plus 2000");
                    Log.d("Harga",String.valueOf(hargaSekarang));

                }else {
                    hargaSekarang = hargaAsliNow;
                    simpanData.put("Harga",String.valueOf(hargaSekarang));
                    simpanData.put("HargaLast",String.valueOf(hargaSekarang));
                    Log.d("Masuk mana","Kumpulan tanpa plus");
                    Log.d("Harga",String.valueOf(hargaSekarang));

                }
                simpanData.put("Rasa",(parent.getItemAtPosition(position)).toString());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Button order = (Button) findViewById(R.id.buttonOrder);
        order.setOnClickListener(new ButtonToppingActivityHandler());

    }


    /**
     * Kelas untuk menghandle Button di tekan untuk activity ini
     */

    private class ButtonToppingActivityHandler implements Button.OnClickListener{
        @Override
        public void onClick(View v) {
            TextView jumlahPesanan = (TextView) findViewById(R.id.jumlahPesanan);
            switch (v.getId()){
                case R.id.buttonIncrement:
                    int curr = Integer.parseInt(jumlahPesanan.getText().toString());
                    jumlahPesanan.setText(String.valueOf(curr+1));
                    break;
                case R.id.buttonDecrement:
                    int current = Integer.parseInt(jumlahPesanan.getText().toString());
                    if (current<=1) current=1;
                    else current-=1;
                    jumlahPesanan.setText(String.valueOf(current));
                    break;
                case R.id.buttonOrder:
                    Intent intent = new Intent(getBaseContext(),SummaryActivity.class);
                    // Data yang ditransfer ke activity Summary
                    String namaEs = simpanData.get("Nama");
                    int jumlah = Integer.parseInt(jumlahPesanan.getText().toString());
                    int hargaEsTemp = Integer.parseInt(simpanData.get("Harga"));
                    hargaEsTemp*=jumlah;
                    String hargaEs = Integer.toString(hargaEsTemp);
                    String rasa = simpanData.get("Rasa");
                    String topping = simpanData.get("Topping");


                    intent.putExtra("NamaEs",namaEs);
                    intent.putExtra("HargaEs",hargaEs);
                    intent.putExtra("Rasa",rasa);
                    intent.putExtra("Topping",topping);

                    startActivity(intent);




                    break;

            }

        }
    }
}
